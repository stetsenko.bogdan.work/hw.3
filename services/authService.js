import User from '../models/User.js';
import Credential from '../models/Credential.js';
import RegistrationCredential from '../models/RegistrationCredential.js';
import bcrypt from 'bcryptjs';
import jwt from 'jsonwebtoken';
import nodemailer from 'nodemailer';
import { createError } from '../createError.js';
import generator from 'generate-password';
export const saveNewUser = async (user) => {
  const { email, role, username } = user;
  const newUser = new User({
    email,
    role,
    username,
  });
  return await newUser.save();
};

export const saveCredential = async (user, _id) => {
  const { email, password } = user;
  const newCredential = new Credential({
    userId: _id,
    email,
    password: await bcrypt.hash(password, 10),
  });
  return await newCredential.save();
};

export const saveRegCredential = async (user, _id) => {
  const { email, role, password } = user;
  const newCredential = new RegistrationCredential({
    userId: _id,
    email,
    role,
    password: await bcrypt.hash(password, 10),
  });
  return await newCredential.save();
};

export const loginUserService = async (req,next) => {
  const findUser = await User.findOne({ email: req.body.email });
  const findCredential = await Credential.findOne({ userId: findUser._id });
  if (!findUser) {
    return next(createError(400));
  }
  const checkPassword = await bcrypt.compare(
    req.body.password,
    findCredential.password
  );
  if (!checkPassword) {
    return next(createError(400));
  }
  const token = jwt.sign({ id: findUser._id }, process.env.KEY);
  return token;
};

export const passwordForgot = async (req) => {
  const credential = Credential.findOne({ userId: req.user.userId });
  const regCredential = RegistrationCredential.findOne({
    userId: req.user.userId,
  });
  if (!credential || !regCredential) {
    next(createError(400));
  }
  async function main() {
    let transporter = nodemailer.createTransport({
      host: 'smtp.gmail.com',
      port: 587,
      secure: false,
      auth: {
        user: 'stetsenko.bogdan.work@gmail.com',
        pass: 'mngbumftkempbnzt',
      },
    });
    let password = generator.generate({
      length: 25,
      numbers: true,
    });
    await credential.updateOne({ password: await bcrypt.hash(password, 10) });
    await regCredential.updateOne({
      password: await bcrypt.hash(password, 10),
    });
    let info = await transporter.sendMail({
      from: '"NEW PASSWORD" stetsenko.bogdan.work@gmail.com',
      to: req.body.email,
      subject: '⚙️ YOUR NEW PASSWORD ⚙️',
      html: `<b>Hello! Your new password is:\n<em style="color:orange">${password}</em></p>`,
    });
  }
  return main;
};
