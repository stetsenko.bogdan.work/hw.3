import { createError } from '../createError.js';
import RegistrationCredential from '../models/RegistrationCredential.js';
import User from '../models/User.js';
import Credential from '../models/Credential.js';
import bcrypt from 'bcryptjs';
export const getUserInfo = async (req, res, next) => {
  const user = await User.findById(req.user.userId);
  if (!user) {
    next(createError(400));
  }
  res.json({ user });
};
export const deleteUser = async (req, res, next) => {
  const user = await User.findById(req.user.userId);
  const cred = await Credential.findOne({ userId: req.user.userId });
  const reg = await RegistrationCredential.findOne({ userId: req.user.userId });
  if (!user) {
    next(createError(400));
  }
  user.deleteOne();
  cred.deleteOne();
  reg.deleteOne();
  res.json({
    message: 'Profile deleted successfully',
  });
};

export const updatePassword = async (req, res, next) => {
  const cred = await Credential.findOne({ userId: req.user.userId });
  const reg = await RegistrationCredential.findOne({ userId: req.user.userId });
  if (!cred || !reg) {
    next(createError(400));
  }
  await cred.updateOne({ password: await bcrypt.hash(req.body.newPassword, 10) });
  await reg.updateOne({ password: await bcrypt.hash(req.body.newPassword, 10) });
  res.json({
    message: 'Password changed successfully',
  });
};
