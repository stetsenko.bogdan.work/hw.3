import express from 'express';
import morgan from 'morgan';
import cors from 'cors';
import mongoose from 'mongoose';
import dotenv from 'dotenv';
import chalk from 'chalk';
import bodyParser from 'body-parser';
import nodemailer from 'nodemailer';
import { userJoiSchema } from './models/User.js';
import User from './models/User.js';
import authRouter from './routers/authRouter.js';
import loadRouter from './routers/loadRouter.js';
import truckRouter from './routers/truckRouter.js';
import userRouter from './routers/userRouter.js';

const app = express();
dotenv.config();
const connectToMongoDB = () => {
  mongoose
    .connect(
      `mongodb+srv://Bohdan:${process.env.PASSWORD}@cluster0.oc3n4.mongodb.net/?retryWrites=true&w=majority`
    )
    .then(() => {
      console.log(chalk.bgWhite.bold.black('Connected to MongoDB!'));
    })
    .catch((error) => {
      console.log(error);
    });
};
function errorHandler(err, req, res, next) {
  const status = err.status || 500;
  const message = err.message || 'String';
  res.status(status).json({ message });
}

app.use(
  morgan(
    ':method :url HTTP/:http-version" :status :res[content-length] - :response-time ms'
  )
);
app.use(cors());
app.use(express.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use('/api/auth', authRouter);
app.use('/api/loads', loadRouter);
app.use('/api/trucks', truckRouter);
app.use('/api/users/me', userRouter);

app.use(errorHandler);

app.listen(8080, () => {
  connectToMongoDB();
  console.log(chalk.bgYellow.bold('Server has been started!'));
});
