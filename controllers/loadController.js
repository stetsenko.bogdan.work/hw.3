import { createError } from '../createError.js';
import Load, { loadJoiSchema } from '../models/Load.js';
import User from '../models/User.js';
import { saveNewLoad } from '../services/loadService.js';
import Truck from '../models/Truck.js';
import {
  getType,
} from '../truckTypes.js';
export const createUserLoad = async (req, res, next) => {
  loadJoiSchema.validateAsync(req.body).catch(() => {
    next(createError(400));
  });
  saveNewLoad(req)
    .then(() => {
      res.json({
        message: 'Load created successfully',
      });
    })
    .catch(() => {
      next(createError(400));
    });
};

export const getUserLoads = async (req, res, next) => {
  const findUser = await User.findById(req.user.userId);
  const { status, limit, offset } = req.query;
  if (findUser.role === 'SHIPPER') {
    const loads = await Load.find({ created_by: req.user.userId })
      .limit(+limit)
      .skip(+offset - 1);
    const newLoads = status
      ? loads.filter((item) => item.status === status)
      : loads;
    if (!loads) {
      next(createError(400));
    }
    res.json({
      loads: newLoads,
    });
  } else {
    if (status) {
      const loads = await Load.find({ created_by: req.user.userId, status })
        .limit(+limit)
        .skip(+offset - 1);
      if (!loads) {
        next(createError(400));
      }
      res.json({
        loads,
      });
    } else {
      const loads = await Load.find({ assigned_to: req.user.userId })
        .limit(+limit)
        .skip(+offset - 1);
      if (!loads) {
        next(createError(400));
      }
      res.json({
        loads,
      });
    }
  }
};

export const getUserActiveLoad = async (req, res, next) => {
  const findUser = await User.findById(req.user.userId);
  if (findUser.role === 'DRIVER') {
    const findLoad = await Load.findOne({
      assigned_to: req.user.userId,
      status: 'ASSIGNED',
    });
    if (!findLoad) {
      next(createError(400));
    } else {
      res.json({
        load: {
          _id: findLoad._id,
          created_by: findLoad.created_by,
          assigned_to: findLoad.assigned_to,
          status: findLoad.status,
          state: findLoad.state,
          name: findLoad.name,
          payload: findLoad.payload,
          pickup_address: findLoad.pickup_address,
          delivery_address: findLoad.delivery_address,
          dimensions: findLoad.dimensions,
          logs: findLoad.logs,
          created_date: findLoad.created_date,
        },
      });
    }
  } else {
    next(createError(400));
  }
};

export const postUserLoadById = async (req, res, next) => {
  const load = await Load.findById(req.params.id);
  if (!load) {
    next(createError(400));
  }
  await load.updateOne({
    $set: {
      status: 'POSTED',
    },
    $push: {
      logs: {
        message: `Load status changed to 'POSTED'`,
        time: new Date().toISOString(),
      },
    },
  });
  let driver_found = false;
  const truck = await Truck.findOne({
    status: 'IS',
    assigned_to: { $ne: null },
  });

  const truckInfo = truck ? getType(truck.type) : null;
  if (truck) {
    if (
      load.payload < truckInfo.capacity &&
      load.dimensions.width < truckInfo.width &&
      load.dimensions.length < truckInfo.length &&
      load.dimensions.height < truckInfo.height
    ) {
      await truck.updateOne({
        $set: { status: 'OL', shipperId: req.user.userId },
      });
      let driver_found = true;
      await load.updateOne({
        $set: {
          status: 'ASSIGNED',
          assigned_to: truck.created_by,
          state: 'En route to Pick Up',
        },
        $push: {
          logs: {
            message: `Load assigned to driver with id ${truck.created_by}`,
            time: new Date(Date.now()),
          },
        },
      });
      res.json({ message: 'Load posted successfully', driver_found });
    }
  } else {
    if (load.status === 'ASSIGNED') {
      next(createError(400));
    }
    await load.updateOne({
      $set: {
        status: 'NEW',
      },
      $push: {
        logs: {
          message: `Load status changed to 'NEW'! Driver not found!`,
          time: new Date().toISOString(),
        },
      },
    });
    res.json({
      message: 'Load posted successfully',
      driver_found,
    });
  }
};

export const getShippingInfo = async (req, res, next) => {
  const load = await Load.findById(req.params.id);
  if (!load || load.status !== 'ASSIGNED') {
    next(createError(400));
  }
  const truck = await Truck.findOne({ shipperId: (await load).created_by });
  console.log(truck._id);
  if (!truck) {
    next(createError(400));
  }
  const { shipperId, ...other } = truck._doc;
  res.json({ load, truck: other });
};

export const deleteUserLoadById = async (req, res, next) => {
  const load = await Load.findById(req.params.id);
  const truck = await Truck.findOne({ shipperId: load.created_by });
  if (!load) {
    next(createError(400));
  }
  load.deleteOne();
  if (truck) {
    await truck.updateOne({
      $set: {
        status: 'IS',
        shipperId: null,
      },
    });
  }
  res.json({
    message: 'Load deleted successfully',
  });
};

export const updateLoad = async (req, res, next) => {
  const load = Load.findById(req.params.id);
  if (!load || Object.keys(req.body).length < 1) {
    next(createError(400));
  } else {
    await load.updateOne(req.body);
    res.json({
      message: 'Load details changed successfully',
    });
  }
};

export const getUserLoadById = async (req, res, next) => {
  const load = await Load.findById(req.params.id);
  if (!load) {
    next(createError(400));
  }
  res.json(load);
};

export const IterateNextState = async (req, res, next) => {
  const driver = await User.findById(req.user.userId);
  if (driver.role !== 'DRIVER') {
    next(createError(400));
  }
  const load = await Load.findOne({
    assigned_to: driver._id,
    status: 'ASSIGNED',
  });
  if (!load) {
    next(createError(400));
  }
  switch (load.state) {
    case 'En route to Pick Up':
      await load.updateOne({
        $set: {
          state: 'Arrived to Pick Up',
        },
        $push: {
          logs: {
            message: `Load state changed to 'Arrived to Pick Up'`,
            time: new Date().toISOString(),
          },
        },
      });
      res.json({
        message: "Load state changed to 'Arrived to Pick Up'",
      });
      break;
    case 'Arrived to Pick Up':
      await load.updateOne({
        $set: {
          state: 'En route to delivery',
        },
        $push: {
          logs: {
            message: `Load state changed to 'En route to delivery'`,
            time: new Date().toISOString(),
          },
        },
      });
      res.json({
        message: "Load state changed to 'En route to delivery'",
      });
      break;
    case 'En route to delivery':
      await load.updateOne({
        $set: {
          state: 'Arrived to delivery',
          status: 'SHIPPED',
        },
        $push: {
          logs: {
            message: `Load state changed to 'Arrived to delivery'`,
            time: new Date().toISOString(),
          },
        },
      });
      const truck = await Truck.findOne({ shipperId: load.created_by });
      await truck.updateOne({
        $set: {
          status: 'IS',
          shipperId: null,
        },
      });
      res.json({
        message: "Load state changed to 'Arrived to delivery'",
      });
      break;
    default:
      break;
  }
};
