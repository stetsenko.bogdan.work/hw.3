import mongoose from 'mongoose';
import Joi from 'joi';

const registrationCredentialJoiSchema = Joi.object({
  password: Joi.string().min(3).alphanum().required(),
});
const RegistrationCredential = new mongoose.Schema({
  email: {
    type: String,
    required: true,
    unique: true,
  },
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
  role: {
    type: String,
    enum: ['DRIVER', 'SHIPPER'],
    required: true,
  },
});
export { registrationCredentialJoiSchema };
export default mongoose.model('RegistrationCredential', RegistrationCredential);
