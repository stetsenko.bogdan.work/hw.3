import mongoose from 'mongoose';
import Joi from 'joi';
const loadJoiSchema = Joi.object({
  name: Joi.string().required(),
  payload: Joi.number().required(),
  pickup_address: Joi.string().min(10).required(),
  delivery_address: Joi.string().min(10).required(),
  dimensions: {
    width: Joi.number().required(),
    length: Joi.number().required(),
    height: Joi.number().required(),
  },
});
const Load = new mongoose.Schema({
  created_by: {
    type: mongoose.Schema.Types.ObjectId,
  },
  assigned_to: {
    type: mongoose.Schema.Types.ObjectId,
  },
  status: {
    type: String,
    enum: ['NEW', 'POSTED', 'ASSIGNED', 'SHIPPED'],
    default: 'NEW',
  },
  state: {
    type: String,
    enum: [
      'En route to Pick Up',
      'Arrived to Pick Up',
      'En route to delivery',
      'Arrived to delivery',
    ],
  },
  name: {
    type: String,
    required: true,
  },
  payload: {
    type: Number,
    requited: true,
  },
  pickup_address: {
    type: String,
    required: true,
  },
  delivery_address: {
    type: String,
    required: true,
  },
  dimensions: {
    width: {
      type: Number,
      required: true,
    },
    length: {
      type: Number,
      required: true,
    },
    height: {
      type: Number,
      required: true,
    },
  },
  logs: {
    type: Array,
    message: { type: String },
    time: { type: String },
    default: {
      message: 'Waiting!',
      time: new Date().toISOString(),
    },
  },
  created_date: {
    type: String,
    default: new Date().toISOString(),
  },
});
export { loadJoiSchema };
export default mongoose.model('Load', Load);
