import {
  saveCredential,
  saveNewUser,
  saveRegCredential,
  loginUserService,
  passwordForgot,
} from '../services/authService.js';
import nodemailer from 'nodemailer';
import { userJoiSchema } from '../models/User.js';
import { createError } from '../createError.js';
import Credential from '../models/Credential.js';
import RegistrationCredential from '../models/RegistrationCredential.js';
import generator from 'generate-password';
import bcrypt from 'bcryptjs';
import { registrationCredentialJoiSchema } from '../models/RegistrationCredential.js';

export const registerNewUser = async (req, res, next) => {
  userJoiSchema
    .validateAsync(req.body)
    .then(
      registrationCredentialJoiSchema.validateAsync({
        password: req.body.password,
      })
    )
    .catch(() => {
      next(createError(400));
    });
  saveNewUser(req.body)
    .then((item) => {
      saveRegCredential(req.body, item._id);
      saveCredential(req.body, item._id);
      res.json({
        message: 'Profile created successfully',
      });
    })
    .catch(400);
};

export const loginUser = async (req, res, next) => {
  const token = await loginUserService(req, next);
  if (token) {
    return res.json({ jwt_token: token });
  }
};

export const forgotPassword = async (req, res, next) => {
  if (!req.body.email) {
    next(createError(400));
  }
  const func = await passwordForgot(req, next);
  await func();
  res.json({
    message: 'New password sent to your email address',
  });
};
